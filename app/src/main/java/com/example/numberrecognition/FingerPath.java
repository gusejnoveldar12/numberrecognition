package com.example.numberrecognition;

import android.graphics.Path;

public class FingerPath {
    public int width;
    public Path path;

    public FingerPath(int width, Path path)
    {
        this.width = width;
        this.path = path;
    }
}
