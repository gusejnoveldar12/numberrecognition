package com.example.numberrecognition;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.Toast;

import com.example.numberrecognition.neuronnetwork.CsvReader;
import com.example.numberrecognition.neuronnetwork.NeuronLayer;
import com.example.numberrecognition.neuronnetwork.NeuronNet;


public class MainActivity extends AppCompatActivity {
    private PaintView paintView;
    boolean PaintViewInit = false;
    NeuronNet network;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        paintView = (PaintView) findViewById(R.id.paintView);
        paintView.getViewTreeObserver().addOnGlobalLayoutListener(new MyGlobalListenerClass());
        networkInit();
    }

    class MyGlobalListenerClass implements ViewTreeObserver.OnGlobalLayoutListener {
        @Override
        public void onGlobalLayout() {
            View v = (View) findViewById(R.id.paintView);
            if (!PaintViewInit) {
                paintView.init(v.getWidth(), v.getHeight());
                PaintViewInit = true;
            }
        }
    }

    public void onClearButtonClick(View view) {
        paintView.clear();
    }

    public void onRecognizeButtonClick(View view) {
        imageProcessor ip = new imageProcessor(paintView.getmBitmap());
        double[] num;
        double[] result;
        num = ip.arrayFromBitmap();
        result = network.feedForward(num);
        int max = 0;
        for (int i = 1; i < 10; i++) {
            if (result[i] > result[max])
                max = i;
        }
        Toast toast = Toast.makeText(getApplicationContext(),
                "Возможно ваша цифра = " + max,
                Toast.LENGTH_LONG);
        toast.setGravity(Gravity.BOTTOM, 0, 0);
        toast.show();
    }

    public void networkInit() {
        NeuronLayer[] layers = new NeuronLayer[4];
        layers[0] = new NeuronLayer(784);
        layers[1] = new NeuronLayer(20, NeuronLayer.SIGMOIDLOG);
        layers[2] = new NeuronLayer(20, NeuronLayer.RELU);
        layers[3] = new NeuronLayer(10, NeuronLayer.SOFTMAX);
        network = new NeuronNet(4, layers, NeuronNet.CROSSENTROPY);

        CsvReader csvReadWeights = new CsvReader("weights.csv", getApplicationContext());
        network.readWeightsFromFile(csvReadWeights);
        csvReadWeights.close();
    }

}
