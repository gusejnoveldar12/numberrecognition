package com.example.numberrecognition;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;

import static android.graphics.Bitmap.createBitmap;
import static android.graphics.Bitmap.createScaledBitmap;


public class imageProcessor {
    public static int outputImageWidth = 28;
    public static int outputImageHeight = 28;
    Paint paint;
    Bitmap sourceBitmap;
    int width, height;
    int left, right, top, bottom;

    imageProcessor(Bitmap bitmap) {
        sourceBitmap = bitmap;
        width = sourceBitmap.getWidth();
        height = sourceBitmap.getHeight();
        left = right = top = bottom = 0;
        paint = new Paint();
        paint.setAntiAlias(true);
        paint.setDither(true);
        paint.setColor(Color.BLACK);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeJoin(Paint.Join.ROUND);
        paint.setStrokeCap(Paint.Cap.ROUND);
        paint.setXfermode(null);
        paint.setAlpha(0xff);
        paint.setStrokeWidth(PaintView.DEFAULT_WIDTH);
        paint.setMaskFilter(null);
    }

    int sumRGBtoInverseGrayscale(int rgb) {
        return 255 - rgb / 3;
    }


    void findBorders() {
        boolean foundLeft = false, foundRight = false;
        boolean foundTop = false, foundBottom = false;

        for (int column = 0; column < width; column++) {
            for (int row = 0; row < height; row++) {
                int sumRGB, color;
                color = sourceBitmap.getPixel(column, row);
                sumRGB = ((color >> 16) & 0xff) + ((color >> 8) & 0xff) + ((color) & 0xff);
                if (sumRGBtoInverseGrayscale(sumRGB) > 0 && !foundLeft) {
                    foundLeft = true;
                    left = column;
                }
                color = sourceBitmap.getPixel(width - 1 - column, row);
                sumRGB = ((color >> 16) & 0xff) + ((color >> 8) & 0xff) + ((color) & 0xff);
                if (sumRGBtoInverseGrayscale(sumRGB) > 0 && !foundRight) {
                    foundRight = true;
                    right = width - 1 - column;
                }
                if (foundLeft && foundRight)
                    break;
            }
            if (foundLeft && foundRight)
                break;
        }



        for (int row = 0; row < height; row++) {
            for (int column = 0; column < width; column++) {
                int sumRGB, color;
                color = sourceBitmap.getPixel(column, row);
                sumRGB = ((color >> 16) & 0xff) + ((color >> 8) & 0xff) + ((color) & 0xff);
                if (sumRGBtoInverseGrayscale(sumRGB) > 0 && !foundTop) {
                    foundTop = true;
                    top = row;
                }
                color = sourceBitmap.getPixel(column,height - 1 - row);
                sumRGB = ((color >> 16) & 0xff) + ((color >> 8) & 0xff) + ((color) & 0xff);
                if (sumRGBtoInverseGrayscale(sumRGB) > 0 && !foundBottom) {
                    foundBottom = true;
                    bottom = height - 1 - row;
                }
                if (foundTop && foundBottom)
                    break;
            }
            if (foundTop && foundBottom)
                break;
        }
    }

    Bitmap createNormalBitmap() {
        findBorders();
        int dx, dy;
        dx = right - left + 1;
        dy = bottom - top + 1;

        Bitmap truncateBitmap = createBitmap(sourceBitmap, left, top, dx, dy);
        Bitmap scaledBitmap;
        int scaledWidth, scaledHeight;
        if (dx >= dy ) {
            scaledWidth = 20;
            scaledHeight = 20 * dy / dx;
            scaledBitmap = createScaledBitmap(truncateBitmap, scaledWidth, scaledHeight, true);
        }
        else {
            scaledWidth = 20 * dx / dy;
            scaledHeight = 20;
            scaledBitmap = createScaledBitmap(truncateBitmap, scaledWidth, scaledHeight, true);
        }

        int scaledCentreX, scaledCentreY, xm = 0, ym = 0, m = 0;
        for (int i = 0; i < scaledWidth; i++)
            for (int j = 0; j <scaledHeight; j++) {
                int sumRGB, color;
                color = scaledBitmap.getPixel(i, j);
                sumRGB = ((color >> 16) & 0xff) + ((color >> 8) & 0xff) + ((color) & 0xff);
                color = sumRGBtoInverseGrayscale(sumRGB);
                if (color > 0) {
                    xm += i * color;
                    ym += j * color;
                    m += color;
                }
            }

        if (m != 0) {
            scaledCentreX = xm / m;
            scaledCentreY = ym / m;
        }
        else {
            scaledCentreX = scaledWidth / 2;
            scaledCentreY = scaledHeight / 2;
        }

        Bitmap result = createBitmap(28, 28, Bitmap.Config.ARGB_8888);

        Canvas canv = new Canvas(result);
        canv.drawColor(Color.WHITE);
        Rect dst;
        Rect src = new Rect(0, 0, scaledWidth, scaledHeight);

        dst = new Rect(13 - scaledCentreX, 13 - scaledCentreY, 13 - scaledCentreX + scaledWidth + 1, 13 - scaledCentreY + scaledHeight + 1);

        canv.drawBitmap(scaledBitmap, src, dst, paint);
        return result;
    }

    int cut(int curCord, int min, int max) {
        if (curCord < min)
            return min;
        else if (curCord > max)
            return max;
        else
            return curCord;
    }

    int getRed(int color) {
        int red = (color >> 16) & 0xff;
        return red;
    }

    int getGreen(int color) {
        int green = (color >> 16) & 0xff;
        return green;
    }

    int getBlue(int color) {
        int blue = (color >> 16) & 0xff;
        return blue;
    }

    int getColorFromRGB(int red, int green, int blue) {
        int result = (255 & 0xff) << 24 | (red & 0xff) << 16 | (green & 0xff) << 8 | (blue & 0xff);
        return result;
    }

    Bitmap blurImage(Bitmap source, int radius) {
        int sourceWidth = source.getWidth();
        int sourceHeight = source.getHeight();

        for (int i = 0; i < sourceWidth; i++)
            for (int j = 0; j < sourceHeight ; j++) {
                double red, green, blue;
                int redSum = 0, greenSum = 0, blueSum = 0, count = 0;
                int leftTopPixelX = cut(i - radius, 0, i);
                int leftTopPixelY = cut(j - radius, 0, j);
                int rightBottomPixelX = cut(i + radius, i, sourceWidth - 1);
                int rightBottomPixelY = cut(j + radius, j, sourceHeight - 1);


                for (int yc = leftTopPixelY; yc <= rightBottomPixelY; yc++)
                    for (int xc = leftTopPixelX; xc <= rightBottomPixelX; xc++) {
                        int color = source.getPixel(xc, yc);
                        redSum += getRed(color);
                        greenSum += getGreen(color);
                        blueSum += getBlue(color);
                        count++;
                    }

                red = redSum / (double)count;
                green = greenSum / (double)count;
                blue = blueSum / (double)count;

                source.setPixel(i, j, getColorFromRGB((int)red, (int)green, (int)blue));
            }
        return source;
    }

    double[] arrayFromBitmap() {
        //Bitmap bitmap = blurImage(createNormalBitmap(), 1);
        Bitmap bitmap = createNormalBitmap();
        double[] result = new double[outputImageWidth * outputImageHeight];
        int k = 0;
        for (int i = 0; i < outputImageHeight; i++)
            for (int j = 0; j < outputImageWidth; j++, k++) {
                int sumRGB, color;
                double outputColor;
                color = bitmap.getPixel(j, i);
                sumRGB = ((color >> 16) & 0xff) + ((color >> 8) & 0xff) + ((color) & 0xff);
                color = sumRGBtoInverseGrayscale(sumRGB);
                outputColor = color / 255.0;
                result[k] = outputColor;
            }
        return result;
    }
}
