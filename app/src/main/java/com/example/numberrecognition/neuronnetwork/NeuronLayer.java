package com.example.numberrecognition.neuronnetwork;


public class NeuronLayer {
    public static final String SIGMOIDLOG = "sigmoidex";
    public static final String RELU = "relu";
    public static final String SOFTMAX = "softmax";
    public static final String NONE = "none";
    public int neuronCount;
    public String actFunc;

    public NeuronLayer(int neuronCount, String actFunc){
        this.neuronCount = neuronCount;
        this.actFunc = actFunc;
    }

    public NeuronLayer(int neuronCount){
        this.neuronCount = neuronCount;
        this.actFunc = NONE;
    }
}
