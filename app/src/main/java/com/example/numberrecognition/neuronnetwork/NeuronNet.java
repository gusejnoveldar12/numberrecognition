package com.example.numberrecognition.neuronnetwork;

import java.util.Random;

public class NeuronNet {
    // Функция ошибки - Евклидова ошибка
    public static final String EUCLIDLOSS = "euclidloss";
    // Функция ошибки - кроссэнтропия
    public static final String CROSSENTROPY = "crossentropy";
    // Название функции ошибки
    private String lossFuncName;
    // Количество слое в нейронной сети
    private int layerCount;
    // Количество матриц весов в нейронной сети. Оно равно layerCount - 1
    private int weightMatrixCount;
    // Массив, хранящий информацию о каждом слое
    // нейронной сети (функцию активации, количество нейронов)
    private NeuronLayer[] networkStruct;
    // Матрица хранящая активированные выходы. Строка - номера слоя, столбец - номер нейрона в слое
    private double[][] neuronOut;
    // Матрица хранящаяя неактивированные выходы. Строка - номера слоя, столбец - номер нейрона в слое
    private  double[][] nonActivatedNeuronOut;
    // Массив матриц весов. Первая координата - номер матрицы,
    // остальные две это строка и столбец элементов самой матрицы
    private double[][][] weightMatrices;

    // Матрицы хранит частные производные нейронов
    private double[][] neuronsdEdy;
    private double[][] neuronsdEdx;

    // Конструктор создает нейросеть, заполняя матрицы весов небольшими дробными случайными числами
    // Так же заполняет матрицу изменений весов нулями
    public NeuronNet(int layerCount, NeuronLayer[] networkStruct, String lossFuncName) {
        this.layerCount = layerCount;
        this.networkStruct = networkStruct;
        this.lossFuncName = lossFuncName;
        weightMatrixCount = layerCount - 1;
        neuronOut = new double[layerCount][];
        neuronsdEdy = new double[layerCount][];
        neuronsdEdx = new double[layerCount][];
        for (int layerInd = 0; layerInd < layerCount; layerInd++) {
            neuronsdEdy[layerInd] = new double[networkStruct[layerInd].neuronCount];
            neuronsdEdx[layerInd] = new double[networkStruct[layerInd].neuronCount];
        }
        nonActivatedNeuronOut = new double[layerCount][];
        weightMatrices = new double[weightMatrixCount][][];
        for (int matrixId = 0; matrixId < weightMatrixCount ; matrixId++) {
            int rowsCount = networkStruct[matrixId + 1].neuronCount;
            int columnsCount = networkStruct[matrixId].neuronCount;
            weightMatrices[matrixId] = new double[rowsCount][columnsCount];
            for (int rowInd = 0; rowInd < rowsCount; rowInd++)
                for (int columnInd = 0; columnInd < columnsCount; columnInd++)
                    weightMatrices[matrixId][rowInd][columnInd] = Math.random() - 0.5;

        }
    }


    // Конструктор создает нейросеть по заданным матрицам весов
    public NeuronNet(int layerCount, NeuronLayer[] networkStruct, double[][][] weightMatrices, String lossFuncName) {
        this.layerCount = layerCount;
        this.networkStruct = networkStruct;
        this.lossFuncName = lossFuncName;
        weightMatrixCount = layerCount - 1;
        neuronOut = new double[layerCount][];
        nonActivatedNeuronOut = new double[layerCount][];
        this.weightMatrices = weightMatrices;
        neuronsdEdy = new double[layerCount][];
        neuronsdEdx = new double[layerCount][];
        for (int layerInd = 0; layerInd < layerCount; layerInd++) {
            neuronsdEdy[layerInd] = new double[networkStruct[layerInd].neuronCount];
            neuronsdEdx[layerInd] = new double[networkStruct[layerInd].neuronCount];
        }
    }

    // ReLU функция активации. Действует на вектор
    private double[] reluVector(double[] inputV, int vRows) {
        double[] result = new double[vRows];
        for (int rowInd = 0; rowInd < vRows; rowInd++)
            if (inputV[rowInd] > 0)
                result[rowInd] = inputV[rowInd];
            else
                result[rowInd] = 0.3 * inputV[rowInd];
        return result;
    }

    // Softmax функция активации, действует на вектор
    private double[] softmax(double[] inputV, int vRows)
    {
        double[] result = new double[vRows];

        double max = inputV[0];
        for (int ind = 1; ind < vRows; ind++)
            if (inputV[ind] > max)
                max = inputV[ind];

        double expSum = 0.0;
        for (int ind = 0; ind < vRows; ind++)
            expSum += Math.exp(inputV[ind] - max);

        for (int rowInd = 0; rowInd < vRows; rowInd++)
            result[rowInd] = Math.exp(inputV[rowInd] - max) / expSum;

        return result;
    }

    // Вектор-функция, возвращающая новый вектор равный действию сигмоиды на старый вектор
    private double[] sigmoidExpVector(double[] inputV, int vRows) {
        double[] result = new double[vRows];
        for (int rowInd = 0; rowInd < vRows; rowInd++)
            result[rowInd] = 1 / (1 + Math.exp(-1*inputV[rowInd]));
        return result;
    }

    // Функция умножающая матрицу на вектор. Результат всегда вектор размера mRows
    private double[] matrixMulVector(double[][] matrix, int mRows, int mCols, double[] vector) {
        double[] resultVector = new double[mRows];
        for (int rowInd = 0; rowInd < mRows; rowInd++)
            for (int colInd = 0; colInd < mCols; colInd++)
                resultVector[rowInd] += matrix[rowInd][colInd] * vector[colInd];
        return resultVector;
    }

    // Функция реализующая прямое распространение в нейронной сети.
    //А так же она заполняет массив значений активированных нейронов neuronOut
    public double[] feedForward(double[] inputV) {
        double[] outputVector = inputV;
        neuronOut[0] = inputV;
        nonActivatedNeuronOut[0] = inputV;
        for (int stepInd = 0; stepInd < weightMatrixCount; stepInd++) {
            int matrixRows = networkStruct[stepInd + 1].neuronCount;
            int matrixColumns = networkStruct[stepInd].neuronCount;
            outputVector = matrixMulVector(weightMatrices[stepInd], matrixRows, matrixColumns, outputVector);
            nonActivatedNeuronOut[stepInd + 1] = outputVector;
            switch (networkStruct[stepInd + 1].actFunc) {
                case NeuronLayer.RELU: outputVector = reluVector(outputVector, matrixRows);break;
                case NeuronLayer.SIGMOIDLOG: outputVector = sigmoidExpVector(outputVector, matrixRows); break;
                case NeuronLayer.SOFTMAX: outputVector = softmax(outputVector, matrixRows); break;
            }
            neuronOut[stepInd + 1] = outputVector;
        }
        return outputVector;
    }

    // Функция возвращает число нейронов на входе или число входов
    public int getInputNeuronCount() {
        return  networkStruct[0].neuronCount;
    }

    // Функция возвращает число нейронов на выходе или число выходов
    public int getOutputNeuronCount() {
        return  networkStruct[layerCount - 1].neuronCount;
    }

    // функция возвращает веса нейросети
    double[][][] getWeightMatrices() {
        return weightMatrices;
    }

    // Функция возварщает структуру нейросети
    NeuronLayer[] getNetworkStruct() {
        return networkStruct;
    }

    // Функция возвращает количество слоев нейросети
    int getLayerCount() {
        return layerCount;
    }

    // Функция записывает веса из нейросети в csv файл
    public void writeWeightsToFile(CsvWriter csv) {
        int limit = 0;
        for (int matrixId = 0; matrixId < weightMatrixCount ; matrixId++) {
            int rowsCount = networkStruct[matrixId + 1].neuronCount;
            int columnsCount = networkStruct[matrixId].neuronCount;
            for (int rowInd = 0; rowInd < rowsCount; rowInd++)
                for (int columnInd = 0; columnInd < columnsCount; columnInd++, limit++) {
                    if (limit < 10) {
                        csv.writeDouble(weightMatrices[matrixId][rowInd][columnInd]);
                        if (limit + 1 != 10)
                            csv.writeComa();
                    }
                    else {
                        limit = 0;
                        csv.writeEnter();
                        csv.writeDouble(weightMatrices[matrixId][rowInd][columnInd]);
                        csv.writeComa();
                    }
                }
        }
    }

    // Функция считывает веса из csv файла в нейросеть
    public void readWeightsFromFile(CsvReader csv) {
        for (int matrixId = 0; matrixId < weightMatrixCount ; matrixId++) {
            int rowsCount = networkStruct[matrixId + 1].neuronCount;
            int columnsCount = networkStruct[matrixId].neuronCount;
            for (int rowInd = 0; rowInd < rowsCount; rowInd++)
                for (int columnInd = 0; columnInd < columnsCount; columnInd++) {
                    weightMatrices[matrixId][rowInd][columnInd] = csv.getNextDouble();
                }
        }
    }
    // Это функция ошибки - перекрестная энтропия
    private double crossEntropyLoss(double[] actual, double[] expected) {
        double result = 0.0;
        int n = getOutputNeuronCount();
        for (int i = 0; i < n; i++) {
            result += expected[i] * Math.log(actual[i]);
        }
        return -result;
    }

    // Это функция ошибки - Евклидова норма
    private double euclidLoss(double[] actual, double[] expected) {
        double result = 0.0;
        for (int i = 0; i < getOutputNeuronCount(); i++)
            result += (expected[i] - actual[i]) * (expected[i] - actual[i]);
        return 0.5 * result;
    }

    // Это производная функции ошибки ( функция ошибка в данном случае это - Евклидова норма)
    private double euclidLossDer(double actual, double expected) {
        return actual - expected;
    }

    // Это проивзодная функции ошибки (функция ошибка в данном случае это - кроссэнтропия. Её производная совпадает с производной евклидовой нормы)
    private double crossEntropyLossDer(double actual, double expected) {
        return actual - expected;
    }

    // Это производная функции-активации (функция-активации в данном случае - логистическая функция)
    private  double logisticDer(double output) {
        return output * (1 - output);
    }

    // Это производная функции-активации (функция-активации в данном случае - ReLU)
    private  double reluDer(double output) {
        if (output > 0)
            return 1.0;
        else
            return 0.3;
    }

    private double softmaxDer(double[] input, int i, int j) {
        if (i == j)
            return input[i] * (1 - input[i]);
        else
            return -input[j] * input[i];
    }

    //Функция выполняет алгоритм обратного распространения ошибки
    public void computeGrad(double[] inputV, double[] expectedOutV) {
        // Вычисление выходов нейросети
        double[] output;
        output = feedForward(inputV);

        // Вычисление ошибок нейронов выходного слоя
        int outputNeuronCount = getOutputNeuronCount();
        int outputLayerInd = layerCount - 1;
        for (int neuronInd = 0 ; neuronInd < outputNeuronCount; neuronInd++) {
                neuronsdEdy[outputLayerInd][neuronInd] = output[neuronInd] - expectedOutV[neuronInd];
                switch(networkStruct[outputLayerInd].actFunc) {
                    case NeuronLayer.SIGMOIDLOG: neuronsdEdx[outputLayerInd][neuronInd] = logisticDer(neuronOut[outputLayerInd][neuronInd]) * neuronsdEdy[outputLayerInd][neuronInd]; break;
                    case NeuronLayer.RELU: neuronsdEdx[outputLayerInd][neuronInd] = reluDer(nonActivatedNeuronOut[outputLayerInd][neuronInd]) * neuronsdEdy[outputLayerInd][neuronInd]; break;
                    case NeuronLayer.SOFTMAX: neuronsdEdx[outputLayerInd][neuronInd] = softmaxDer(neuronOut[outputLayerInd], neuronInd, neuronInd) * neuronsdEdy[outputLayerInd][neuronInd]; break;
                }
        }

        // Вычисление ошибок нейроннов внутреннних слоев
        for (int layerInd = layerCount - 2; layerInd > 0; layerInd--) {
            int innerNeuronCount = networkStruct[layerInd].neuronCount;
            for (int neuronInd = 0; neuronInd < innerNeuronCount; neuronInd++) {
                // Вычислим произведения весов на ошибки нейронов следующего слоя связанных с данным нейроном текущего слоя
                double sumDeltaWeight = 0;
                for (int ind = 0; ind < networkStruct[layerInd + 1].neuronCount; ind++)
                    sumDeltaWeight += weightMatrices[layerInd][ind][neuronInd] * neuronsdEdx[layerInd + 1][ind];

                // Вычислим ошибку текущего нейрона на текущем слое
                neuronsdEdy[layerInd][neuronInd] = sumDeltaWeight;
                switch (networkStruct[layerInd].actFunc) {
                    case NeuronLayer.SIGMOIDLOG: {
                        neuronsdEdx[layerInd][neuronInd] = logisticDer(neuronOut[layerInd][neuronInd]) * neuronsdEdy[layerInd][neuronInd];
                        break;
                    }
                    case NeuronLayer.RELU: {
                        neuronsdEdx[layerInd][neuronInd] = reluDer(nonActivatedNeuronOut[layerInd][neuronInd]) * neuronsdEdy[layerInd][neuronInd];
                        break;
                    }
                }
            }
        }

        /*
        // Скорректируем веса при этом сохранив их изменения
        for (int matrInd = 0; matrInd < weightMatrixCount; matrInd++)
            for (int neuronInd = 0; neuronInd < networkStruct[matrInd].neuronCount; neuronInd++)
                for (int nextNeuronCount = 0; nextNeuronCount < networkStruct[matrInd + 1].neuronCount; nextNeuronCount++) {
                    double curDeltaWeight = learnRate * neuronOut[matrInd][neuronInd] * neuronsDelta[matrInd + 1][nextNeuronCount];
                    weightMatrices[matrInd][nextNeuronCount][neuronInd] -= (curDeltaWeight - moment * weightDelta[matrInd][nextNeuronCount][neuronInd]);
                    weightDelta[matrInd][nextNeuronCount][neuronInd] = curDeltaWeight;
                }
*/
    }


    // Оптимизатор Adam (Adaptive moment estimation)
    public void adam(double[][] inpMTrain, double[][] expectedOutMTrain, int countTrain, double[][] inpMTest, double[][] expectedOutMTest, int countTest,  int iterCount, int batchSize, double learnRate, double betta1, double betta2, double eps) {

        double m = 0, mc = 0;
        double v = 0, vc = 0;
        for (int iter = 0; iter < iterCount; iter++ ) {
            double loss = 0.0;
            double[] networkResult;
            for (int i = 0; i < countTest; i++) {
                networkResult = feedForward(inpMTest[i]);
                if (lossFuncName == EUCLIDLOSS)
                    loss += euclidLoss(networkResult, expectedOutMTest[i]);
                else if (lossFuncName == CROSSENTROPY)
                    loss += crossEntropyLoss(networkResult, expectedOutMTest[i]);
            }
            loss /= countTest;
            System.out.println("Loss = " + loss);
            for (int elemInd = 0; elemInd < batchSize; elemInd++) {
                computeGrad(inpMTrain[elemInd], expectedOutMTrain[elemInd]);
                for (int matrInd = 0; matrInd < weightMatrixCount; matrInd++)
                    for (int neuronInd = 0; neuronInd < networkStruct[matrInd].neuronCount; neuronInd++)
                        for (int nextNeuronCount = 0; nextNeuronCount < networkStruct[matrInd + 1].neuronCount; nextNeuronCount++) {
                            m = betta1 * m + (1 - betta1) * neuronsdEdx[matrInd + 1][nextNeuronCount] * neuronOut[matrInd][neuronInd];
                            v = betta2 * v + (1 - betta2) * Math.pow(neuronsdEdx[matrInd + 1][nextNeuronCount] * neuronOut[matrInd][neuronInd], 2);
                            mc = m / (1 - Math.pow(betta1, elemInd + 1));
                            vc = v / (1 - Math.pow(betta2, elemInd + 1));
                            weightMatrices[matrInd][nextNeuronCount][neuronInd] -= (learnRate * mc) / (Math.sqrt(vc) + eps);
                        }
            }

            Random r = new Random();
            for (int i = 0; i < countTrain; i++)
            {
                int j = r.nextInt(countTrain);
                double[] buff = inpMTrain[i];
                inpMTrain[i] = inpMTrain[j];
                inpMTrain[j] = buff;

                buff = expectedOutMTrain[i];
                expectedOutMTrain[i] = expectedOutMTrain[j];
                expectedOutMTrain[j] = buff;
            }
        }
    }


    // Метод градиентного спуска
    public void Learn(double[][] inpMTrain, double[][] expectedOutMTrain, int countTrain, double[][] inpMTest, double[][] expectedOutMTest, int countTest, double learnRate, int epoch) {
        double deltaLoss = 0.0;
        for (int epochInd = 0; epochInd < epoch; epochInd++) {
            double loss = 0.0;
            double[] networkResult;
            for (int i = 0; i < countTest; i++) {
                networkResult = feedForward(inpMTest[i]);
                if (lossFuncName == EUCLIDLOSS)
                    loss += euclidLoss(networkResult, expectedOutMTest[i]);
                else if (lossFuncName == CROSSENTROPY)
                    loss += crossEntropyLoss(networkResult, expectedOutMTest[i]);
            }
            loss /= countTest;
            if (loss <= 0.000001)
                break;

            for (int elemInd = 0; elemInd < countTrain; elemInd++) {
                computeGrad(inpMTrain[elemInd], expectedOutMTrain[elemInd]);
                for (int matrInd = 0; matrInd < weightMatrixCount; matrInd++)
                    for (int neuronInd = 0; neuronInd < networkStruct[matrInd].neuronCount; neuronInd++)
                        for (int nextNeuronCount = 0; nextNeuronCount < networkStruct[matrInd + 1].neuronCount; nextNeuronCount++) {
                            weightMatrices[matrInd][nextNeuronCount][neuronInd] -= learnRate * neuronsdEdx[matrInd + 1][nextNeuronCount] * neuronOut[matrInd][neuronInd];
                        }
            }
            System.out.println(epochInd + 1 + " loss = " + loss);
        }
    }
}
