package com.example.numberrecognition.neuronnetwork;

import android.content.Context;
import android.content.res.AssetManager;

import java.util.Scanner;
import java.io.*;

public class CsvReader {
    private InputStream is;
    private  Scanner scan;
    private boolean success;

    public CsvReader(String pathToFile, Context context) {
        try {
            is = context.getAssets().open(pathToFile);
            success = true;
            scan = new Scanner(is);
            scan.useDelimiter("[,\n]");
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
            success = false;
        }
    }

    public int getNextInt() {
        int result = -1;
        if (!success)
            return result;
        if (scan.hasNext())
            return Integer.parseInt(scan.next());
        return result;
    }

    public double getNextDouble() {
        double result = -1.0;
        if (!success)
            return result;
        if (scan.hasNext())
            return Double.parseDouble(scan.next());
        return result;
    }

    public void close()
    {
        scan.close();
        try {

            is.close();
        } catch (IOException exc) {
            System.out.println(exc.getMessage());
        }
    }
}
