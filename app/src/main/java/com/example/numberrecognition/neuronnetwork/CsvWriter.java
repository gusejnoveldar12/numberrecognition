package com.example.numberrecognition.neuronnetwork;

import java.io.FileWriter;
import java.io.IOException;

public class CsvWriter {
    private FileWriter file;
    private boolean success;

    public CsvWriter(String pathToFile) {
        try {
            file = new FileWriter(pathToFile);
            success = true;
        } catch (IOException exc) {
            System.out.println(exc.getMessage());
            success = false;
        }
    }

    public void writeInt(int num) {
        if (!success)
            return;
        try {
            file.write(Integer.toString(num));
        } catch (IOException exc) {
            System.out.println(exc.getMessage());
        }
    }

    public void writeDouble(double num) {
        if (!success)
            return;
        try {
            file.write(Double.toString(num));
        } catch (IOException exc) {
            System.out.println(exc.getMessage());
        }
    }

    public void writeComa() {
        if (!success)
            return;
        try {
            file.write(",");
        } catch (IOException exc) {
            System.out.println(exc.getMessage());
        }
    }

    public void writeEnter() {
        if (!success)
            return;
        try {
            file.write("\n");
        } catch (IOException exc) {
            System.out.println(exc.getMessage());
        }
    }

    public void close()
    {
        try {
            file.close();
        } catch (IOException exc) {
            System.out.println(exc.getMessage());
        }
    }

}
